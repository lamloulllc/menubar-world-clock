const { app, Tray, Menu, nativeImage } = require('electron')
const moment = require('moment');
const moment_tz = require('moment-timezone')

let tray

const all_timezones = [
  { name: 'Hong Kong', tz: 'Asia/Hong_Kong', checked: true},
  { name: 'London', tz: 'Europe/London', checked: true},
  { name: 'New York', tz: 'America/New_York', checked: true},
  { name: 'Paris', tz: 'Europe/Paris' },
  { name: 'Seoul', tz: 'Asia/Seoul'},
  { name: 'Taipei', tz: 'Asia/Taipei'},
  { name: 'Tokyo', tz: 'Asia/Tokyo' }
]

const updateTime = () => {
  const times = all_timezones.filter(tz => tz.checked == true).map(tz => moment().tz(tz.tz).format('z HH:mm'))
  tray.setTitle(times.join('  '))
}

app.whenReady().then(() => {
  const icon = nativeImage.createEmpty()
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Select timezones',
      submenu: all_timezones.map(tz => ({
        label: tz.name,
        click: () => {
          tz.checked = !tz.checked
          updateTime()
        },
        type: 'checkbox',
        checked: tz.checked
      }))
    },
    {
      type: 'separator'
    },
    {
      label: 'Quit',
      click: () => app.quit()
    }
  ])

  tray = new Tray(icon)
  tray.setContextMenu(contextMenu)
  setInterval(updateTime, 1000)
})
