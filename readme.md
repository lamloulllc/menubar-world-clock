# Display world time in macOS top menubar

Create a native macOS application to display custom world clock in macOS top menubar using Electron.

### Preview
![](preview.png)

### For development use

Config timezone list in `all_timezones` in main.js
```
const all_timezones = [
  { name: 'Hong Kong', tz: 'Asia/Hong_Kong', checked: true},
  { name: 'London', tz: 'Europe/London', checked: true},
  { name: 'New York', tz: 'America/New_York', checked: true},
  { name: 'Paris', tz: 'Europe/Paris' },
  { name: 'Seoul', tz: 'Asia/Seoul'},
  { name: 'Taipei', tz: 'Asia/Taipei'},
  { name: 'Tokyo', tz: 'Asia/Tokyo' }
]
```

Run the app:

`npm start`

### Build

1. Install Electron packager:  
  `npm install --save-dev electron-packager`

2. Create a icon file for macOS 
   
   Create icon file named "icon.icns"

3. Build the app using Electron packager:
  
    3.1 For Apple M1

    `electron-packager . menubar-world-clock --platform=darwin --arch=arm64 --icon=icon.icns`

    In package.json, you may need to use

    `"start": "electron . --arch=arm64`

    3.2. For Apple Intel

    `electron-packager . menubar-world-clock --platform=darwin --arch=x64 --icon=icon.icns`

    Then the app will be in new folder "menubar-worldclock-darwin-arm64"